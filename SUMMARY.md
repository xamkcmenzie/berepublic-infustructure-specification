# Summary

* [Introduction](README.md)

* [Team Autonomy](content/team-autonomy.md)
  * [Issue and Task Syncing](content/issue-task-syncing.md)
  * [GitLab Mirrors](content/gitlab-mirrors.md)
  * [Trello Intergration Spec](content/trello-intergration.md)
  * [BitBucket Intergration Spec](content/bitbucket-intergration.md)
  * [Jira Intergration Spec](content/jira-intergration.md)
  * [Sentry Error Logging](content/sentry-error-loggin.md)
* [CI/CD](content/ci-cd.md)
  * [Jenkins](content/jenkins-ci.md)
  * [BuddyCI](content/buddy-ci.md)
  * [GitLab](content/gitlab-ci.md)
  * [Environments and Review Apps](content/env-review.md)
  * [Code Quality via SonarCube](content/dummy.md)

## UnDrafted

* [Support](content/dummy.md)
  * [Internal Tools Support](content/dummy.md)
  * [GitLab Groups, Subgroups and forking](content/dummy.md)
  * [Team and Client Service Desk](content/dummy.md)
* [Infrastructure](content/dummy.md)
* [Documentation](content/dummy.md)
* [Zero down-time Beanstalk Migration](content/dummy.md)
* [Internal Workflows](content/dummy.md)
  * [Project/Client Channels With GitLab Bot]
  * [Developer Workflows](content/dummy.md)
  * [Git Branching](content/dummy.md)
  * [Non-developer Access](content/dummy.md)
  * [Content Creators](content/dummy.md)
  * [Project Managers](content/dummy.md)
  * [Account Managers](content/dummy.md)
  * [Client Collaboration](content/dummy.md)
* [End Notes](content/dummy.md)
* [Glossary](GLOSSARY.md)