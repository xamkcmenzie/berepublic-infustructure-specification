const path = require('path');
const Express = require('express');

// Initialize the app.
const app = new Express();

app.use(function(req, res, next) {
  next();
});

// Host the book.
app.use(Express.static(path.join(__dirname, './_book')));

// Start the server.
const port = process.env.PORT || 4001;
app.listen(port, (error) => {
  (error) ? console.log(error) : console.log('Listening on http://localhost:' + port);
});