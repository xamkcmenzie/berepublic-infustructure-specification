# Gitlab Runner (CI)

This documentation has been built tested and deployed using the GitLab Pipelines.

<img src="../imgs/demo-screenshot-2.png" width="100%">

- We first build the app and install the node modules. 
- Then move on to run the tests. Which for the moment are just dummy tests?
- It then runs the project through SonarCube https://sonarcube.seam.es
- Finally, it then Pushes to the Heroku environments

- DEV: https://br-inf-str-docs-demo-dev.herokuapp.com/
- PROD: https://br-inf-str-docs-demo-prod.herokuapp.com/
- PRE: https://br-inf-str-docs-demo-pre-prod.herokuapp.com/
- QA: https://br-inf-str-docs-demo-qa.herokuapp.com/

<img src="../imgs/demo-screenshot-19.png" width="100%">

Setting up the steps is pretty easy and you simply add all the commands to the `.gitlab-ci.yml` file.

```bash
image: node:latest

stages:
  - build
  - test
  - review
  - deploy
  - release
  - cleanup

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/

build:
  stage: build
  variables:
    NODE_ENV: development
  script:
    - npm i && npm run build
  only:
    - branches

unit_test:
  stage: test
  variables:
    NODE_ENV: test
  script:
    - npm i && npm run test:unit
  only:
    - branches

ui_test:
  stage: test
  variables:
    NODE_ENV: test
  script:
    - echo "npm i && npm run test:ui"
  only:
    - branches
```

These then select which jobs run against which type of branches and for each commit runs another pipeline

<img src="../imgs/demo-screenshot-8.png" width="100%">

GitLab has its own integrations for Google Hangouts Chat and Slack so you can have all of this progress post into the projects group chat.

<img src="../imgs/demo-screenshot-13.png" width="100%">