# BuddyCI

BuddyCI is an interesting take on CI CD applications and focuses on ease of use and flexibility. For example, you can boot review apps and deploy WordPress websites. Orchestrate Kubernetes deployments, or simply run a gulp project and send an email.

It's mentioned earlier in this documentation due to its very good git repo syncing.

https://buddy.works/

Its integrations with the Pipelines of both GitLab and BitBucket are fantastic and even display all the correct merge request pipelines status'