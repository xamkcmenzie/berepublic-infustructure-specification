# Trello Integration Spec

Below are the tasks required to integrate 
Trello into this proposed ecosystem.

- Unito Sync
  - BitBucket Group to Trello Board
  - GitLab Group to Trello Board
  - BitBucket Trello Card Integration