# Jenkins Integration

Demo: https://jenkins.seam.es

This Projects CI/CD is set to fire a Jenkins Pipeline on commit. This is done via GitLab but can also be fired with a BitBucket Mirror.

Integrations exist for

BitBucket
https://wiki.jenkins.io/display/JENKINS/BitBucket+Plugin

GitLab
https://wiki.jenkins.io/display/JENKINS/GitLab+Plugin