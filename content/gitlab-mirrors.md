# Gitlab Mirrors

GitLab has really good Mirroring systems set up. This allows you to set upstream and downstream mirrors to multiple other systems and any https based git system. Allowing for a mirror of Beanstalk even though an integration is not present.

<img src="https://docs.gitlab.com/ee/workflow/repository_mirroring/repository_mirroring_new_project.png" width="100%">

<a href="https://docs.gitlab.com/ee/workflow/repository_mirroring.html">
More information is avalible on the Gitlab site:</a>

Mirroring is a good way to have a copy based on another system. For example, a team can work in BitBucket and we can create a downstream copy in Gitlab where any core services can be used if needed. 

Or the whole project can be worked on in BitBucket and GitLab simply left to one side as a mirror

## Drawbacks

However nice this is there is a drawback. The user committing each way needs to be a bot user, this means you end up dropping the history of authors as you sync pushes downstream.

This is a painful drawback. However, another service exists called BuddyCI. Which offers a much better. Git history preserving git repository synchronisation.

<a href="https://buddy.works/">https://buddy.works/</a>