# CI/CD Continuous Intergration, Continuous Deployment

GitLab by default ships with a CI built in. But by far the coolest feature of the GitLab CI. Is that you don't have to use the Gitlab CI.

It allows you to integrate the CI of your choice as well as multiple other services to run alongside the CI.

This allows us to maintain GitLab running Jenkins or Bitbucket running Jenkins. Or any other service we deem necessary to integrate with via git and or webhooks.