# Issue and Task Syncing

<img src="../imgs/demo-screenshot-18.gif" width="100%">

Issue and task syncing can be done via an API. But its a large task with a lot of work to get a good result.

However, Thankfully there is an App called Unito Sync

https://unito.io/

This app is excellent. It hooks into the multiple system, projects. Syncronising all the tasks, issues and comments

Given the number of users, we would be looking at around 200-300 dollars a month for an account. But its so worth it!

This includes Trello, GitHub, GitLab, Jira and Bitbucket.

The graphic on the home page of unit sync does a pretty good job of explaining the issue it solves

<img src="https://unito.io/app/themes/unito/assets/img/dolls_all3.png" width="100%">