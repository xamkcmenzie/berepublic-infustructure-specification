# Team Autonomy

<img src="../imgs/demo-screenshot-5.png" width="100%">

Each individual team handles there own clients and workload, not only that but the teams are split based on multiple disciplines. Mobile development, Web Development, CRM management, Content etc.

Because of this The autonomy of each team and the systems they choose to use is an important difference that needs to be maintained.

It's tempting to think there is that one app that will solve all the issues, but this is simply not the case.

> I'm not sure I would ever want that to be the case.

## Allowing for multiple systems

As such each team may need an additional system for a specific task. or, may just feel more comfortable with the system they already use. Such as BitBucket, Trello, Zoho and more.

This document proposes using all of these systems in line with a master repository index on GitLab. Bitbucket can continue to be the driving force behind the repo, and be mirrored upstream.

> DEMO: This has been demonstrated with this documentation repository it is mirrored downstream to BitBucket and the issues are syncronised using Unito.

You can view the GitLab Repo here
https://gitlab.com/maxmckenzie/berepublic-infustructure-specification

and the BitBucket Mirror Repo here
https://bitbucket.org/xamkcmenzie/berepublic-infustructure-specification/