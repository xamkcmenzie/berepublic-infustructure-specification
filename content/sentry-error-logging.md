# Sentry Tracking

With all the applications and environments at play. It is advisable to have a central location where errors are collected. Sentry is by far the most up to date and most well-supported system.

It allows for error logging on both the front end and the back end to be reported to a central location https://sentry.io

It also supports a massive array of platforms.

- **JavaScript**: Support for React, Angular, Angular 2, Ember, Backbone, and Vue.
- **Node**: Support for Express and Koa.
- **Python**: Support for Django, Flask, Tornado, Celery, RQ, Bottle, Pyramid, and Pylons.
- **PHP**: Support for Monolog, Laravel, and Symfony.
- **Ruby**: Support for Rails and Sinatra.
- **Go**: Integration with the native net/http module.
- **Cocoa**: Support for tvOS, macOS and iOS Objective-C and Swift.
- **Android**: Client available on Maven Central.
- **Java**: Support for Log4j, Log4j 2, Logback, and Google App Engine.
- **Kotlin**: Client available on Maven Central.
- **Scala**: Client available on Maven Central.
- **.NET**: Support for .NET and C#.
- **Perl**: Client available on CPAN.
- **Elixir**: Client available via Hex.

These errors are reported with versioning, user profiles, browser info and environment. It is by far the best way to get ahead of errors.

There are also other services available for this. Such as

- Rollbar: https://rollbar.com/
- OverOps: https://www.overops.com/
- Airbrake: https://airbrake.io/
- Raygun: https://raygun.com/