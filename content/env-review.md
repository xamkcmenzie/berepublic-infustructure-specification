# Environments and Review Apps

Each Environment is deployed to from either master or develop.

The following environments are auto-deployed to Heroku
- DEV: https://br-inf-str-docs-demo-dev.herokuapp.com/
- PROD: https://br-inf-str-docs-demo-prod.herokuapp.com/
- PRE: https://br-inf-str-docs-demo-pre-prod.herokuapp.com/
- QA: https://br-inf-str-docs-demo-qa.herokuapp.com/

This can be deployed to any hosting platform via the CI and install any CLI tools necessary.

## Review Apps
We also have the ability with GitLab to boot review apps using Google Cloud Platform and Kubernetes. They make it incredibly easy with a single button in the project to log in to GCP and boot a cluster automatically.

<img src="../imgs/demo-screenshot-15.png" width="100%">

<img src="../imgs/demo-screenshot-17.png" width="100%">

Review Apps are then available on the Merge request. This means that the QA's and any other stakeholders can review features whilst being totally separated from all the other merge requests and feature development going on. This gives the advantage of having an asynchronous development cycle. and prevents a failer in one released feature blocking the release of all the other features.

<a href="https://docs.gitlab.com/ee/ci/review_apps/">More info can be found on gitlab</a> 

<img src="https://docs.gitlab.com/ee/ci/review_apps/img/review_apps_preview_in_mr.png" width="100%">