# BitBucket Intergration Spec

- Unito Sync
  - Projects to Multiple Trello Boards
  - BitBucket issues and comments synced with GitLab Issues and service desk
  - BitBucket issues and comments synced with Jira Issues
  - BitBucket Trello Card Integration
- GitLab Mirror
  - Option 1: Buddy CI
  - Option 2: GitLab Bot