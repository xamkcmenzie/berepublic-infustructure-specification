# BeRepublic Infrastructure Specification

This specification explains a proposed solution for BeRepublic's internal infrastructure. From Workflows to Auto deployments and app submissions.

This documentation is based on GitBook and Auto deployed with GitLab.com

You can View The GitLab Source here
https://gitlab.com/maxmckenzie/berepublic-infustructure-specification/

## Project Syncronisation and Multiple System Support
All aspects of this documentation are demonstrated in the repository for this documentation That includes
<br><br>
<div style="display: block; margin:0 auto; text-align: center;">
  <img src="imgs/Bitbucket-blue.svg" style="max-height:4rem;padding:0.5rem;display:inline-block;max-width:15rem;">
  <img src="imgs/Jira-Software-blue.svg" style="max-height:4rem;padding:0.5rem;display:inline-block;max-width:15rem;">
  <img src="imgs/Trello-icon-blue.svg" style="max-height:4rem;padding:0.5rem;display:inline-block;max-width:10rem;">
  <br>
  <img src="imgs/gitlab.svg" style="max-height:4rem;display:inline-block;max-width:15rem;">
  <img src="imgs/sonarcube.svg" style="max-height:4rem;display:inline-block;max-width:15rem;">
  <img src="imgs/sentry-logo-black.svg" style="max-height:4rem;display:inline-block;max-width:15rem;">
  <img src="imgs/jenkins.png" style="max-height:4rem;display:inline-block;max-width:15rem;">
</div>
<br>

A Bitbucket mirror that syncronised issues, comments and code between systems.
https://bitbucket.org/xamkcmenzie/berepublic-infustructure-specification/src/master/

A Jenkins CI Instance hosted on GCP (Google Cloud Platform)
https://jenkins.seam.es

A SonarCube Instance hosted on GCP (Google Cloud Platform)
https://sonarcube.seam.es

And an account on unito.io to sync issues between BitBucket, Jira, GitLab, Trello and more
https://app.unito.io


<!-- ![alt text](imgs/demo-screenshot-1.gif "title") -->

## Environments and Review Apps
The Branches and merge requests of this documentation that your reading right now. Is auto deployed to Kubernetes on GCP (Google Cloud Platform) for review apps.

The following environments are also auto deployed to Heroku
- DEV: https://br-inf-str-docs-demo-dev.herokuapp.com/
- PROD: https://br-inf-str-docs-demo-prod.herokuapp.com/
- PRE: https://br-inf-str-docs-demo-pre-prod.herokuapp.com/
- QA: https://br-inf-str-docs-demo-qa.herokuapp.com/

<!-- The below badges display results from GitLab CI and SonarCube

[![pipeline status](https://gitlab.com/maxmckenzie/berepublic-infustructure-specification/badges/master/pipeline.svg)](https://gitlab.com/maxmckenzie/berepublic-infustructure-specification/commits/master)
[![coverage report](https://gitlab.com/maxmckenzie/berepublic-infustructure-specification/badges/master/coverage.svg)](https://gitlab.com/maxmckenzie/berepublic-infustructure-specification/commits/master)
[![SonarCube](https://sonarcube.seam.es/api/project_badges/quality_gate?project=berepublic-infustructure-specification)]
[![Code Smells](https://sonarcube.seam.es/api/project_badges/measure?project=berepublic-infustructure-specification&metric=code_smells)]
[![Maintanability rating](https://sonarcube.seam.es/api/project_badges/measure?project=berepublic-infustructure-specification&metric=sqale_rating)] -->

# Sources and Additional Info
https://medium.com/@speedforcerun/sonarqube-with-gitlab-ci-setup-stepbystep-dotnetcore-version-ee555d37d52e